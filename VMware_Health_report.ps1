# Title:	VMware health check 
# Filename:	healthcheck.sp1       	
# Originally Written by	Ivo Beerens ivo@ivobeerens.nl
# Edited by Darren Bloomfield	
#
# 					
# Description:	Scripts that checks the status of a VMware      
# environment on the following point:		
# - VMware ESX server Hardware and version	       	
# - VMware VC version				
# - Active Snapshots				
# - CDROMs connected to VMs					
# - Datastores and the free space available	
# - VM information such as VMware tools version,  
#   processor and memory limits										


# VMware VirtualCenter server name #

$vcserver= @(
    "nl1imcvcsa01.mgmt.global",
    "nl2imcvcsa02.mgmt.global",
    "nl2imcvcsa01.mgmt.global"
); 

#Email settings - CHANGE the FROM and TO to your own email address
$smtpserver = "10.231.128.1"
$from = "your email address here"
$to = "your email address here"
$servername = $env:COMPUTERNAME
$subject="IMC VMware Health Check Report"

#Header for table, colour, formatting
$Header = @"
<style>
TABLE {border-width: 1px; border-style: solid; border-color: black; border-collapse: collapse;}
TH {border-width: 1px; padding: 3px; border-style: solid; border-color: black; background-color: #6495ED;}
TD {border-width: 1px; padding: 3px; border-style: solid; border-color: black;}
</style>
"@

#Connect to Vcenters in NL1,NL2 and NL2 Legacy
connect-VIServer $vcserver 

# Variables #

$filelocation="c:\VMReports\VMbuild.htm"
$vcversion = get-view serviceinstance
$snap = get-vm | get-snapshot
$date=get-date


# Add Text to the HTML file #

ConvertTo-Html –title "Inlumi IMC VMware report " –body "<H1>Inlumi IMC VMware report</H1>" -head $header "<link rel='stylesheet' href='style.css' type='text/css' />" | Out-File $filelocation
ConvertTo-Html –title "Inlumi IMC VMware report " –body "<H4>Date and time</H4>",$date -head $header "<link rel='stylesheet' href='style.css' type='text/css' />" | Out-File -Append $filelocation
ConvertTo-Html –title "Inlumi IMC VMware report " –body "<H4>VMWare vcenter $vcserver</H4>" -head $header "<link rel='stylesheet' href='style.css' type='text/css' />" | Out-File -Append $filelocation

# VMware VCenter version  #

$vcversion.content.about | select Version, Build, FullName | ConvertTo-Html –title "VMware VCenter Version" –body "<H2>VMware VCenter Version.</H2>" -head "<link rel='stylesheet' href='style.css' type='text/css' />" |Out-File -Append $filelocation



# VMware ESX hardware #

Get-VMHost | Get-View | ForEach-Object { $_.Summary.Hardware } | Select-object Vendor, Model, MemorySize, CpuModel, CpuMhz, NumCpuPkgs, NumCpuCores, NumCpuThreads, NumNics, NumHBAs | ConvertTo-Html –title "VMware ESX server Hardware configuration" –body "<H2>VMware ESX server Hardware configuration.</H2>" -head "<link rel='stylesheet' href='style.css' type='text/css' />" | Out-File -Append $filelocation


# VMware ESX versions #


$clusters = Get-Cluster | Sort Name

ForEach ($cluster in $clusters)
{
	$vmhosts = Get-VMHost -Location $cluster
	
	ForEach ($VMhostView in ($vmhosts | Get-View))
	{
		$TotalHostMemory += $vmhostView.Hardware.MemorySize
	}
	
	$vmhosts | Sort Name -Descending | % { $server = $_ |get-view; $server.Config.Product | select { $server.Name }, Version, Build, FullName }| ConvertTo-Html –body "<H3>$cluster Cluster </H3>" -head "<link rel='stylesheet' href='style.css' type='text/css' />" | Out-File -Append $filelocation
	
	$NumHosts = ($vmhosts | Measure-Object).Count 
	
	$vms = Get-VM -Location $cluster | Where {$_.PowerState -eq "PoweredOn"}
	$NumVMs = $vms.Length
	$TotalRAM_GB = [math]::Round($TotalHostMemory/1GB,$digits)
	
	$TotalVMMemoryMB = $vms | Measure-Object -Property MemoryMB -Sum
	$AssignedRAM_GB = [math]::Round($TotalVMMemoryMB.Sum/1024,$digits)
	$PercentageUsed = [math]::Round((($TotalVMMemoryMB.Sum/1024)/($TotalHostMemory/1GB))*100)		
	
	ConvertTo-Html –body " $NumHosts host(s) running $NumVMs virtual machines" -head "<link rel='stylesheet' href='style.css' type='text/css' />" | Out-File -Append $filelocation
	ConvertTo-Html –body "Total memory resource = $TotalRAM_GB GB"  | Out-File -Append $filelocation
	ConvertTo-Html –body "Total Amount of assigned memory = $AssignedRAM_GB GB"  | Out-File -Append $filelocation
	ConvertTo-Html –body "Memory resource percenatge allocated = $PercentageUsed %"  | Out-File -Append $filelocation
	
	Clear-Variable vmhosts -ErrorAction SilentlyContinue
	Clear-Variable vms -ErrorAction SilentlyContinue
	Clear-Variable NumVMs -ErrorAction SilentlyContinue
	Clear-Variable TotalHostMemory -ErrorAction SilentlyContinue
	Clear-Variable TotalVMMemoryMB -ErrorAction SilentlyContinue
}


# Snapshots # 

$snap | select vm, name,created,description | ConvertTo-Html –title "Snaphots active" –body "<H2>Snapshots Active</H2>" -head "<link rel='stylesheet' href='style.css' type='text/css' />"| Out-File -Append $filelocation


# VMware CDROM connected to VMs # 

Get-vm | where { $_ | get-cddrive | where { $_.ConnectionState.Connected -eq "true" } } | Select Name | ConvertTo-Html –title "CDROMs connected" –body "<H2>CDROM's Connected</H2>" -head "<link rel='stylesheet' href='style.css' type='text/css' />"|Out-File -Append $filelocation


# VM information #

$Report = @()
 
get-vm | Sort Name -Descending | % {
  $vm = Get-View $_.ID
    $vms = "" | Select-Object VMName, Hostname, Cluster, OverallCpuUsage, VMState, TotalCPU, TotalMemory, MemoryUsage, TotalNics, ToolsStatus, ToolsVersion
    $vms.VMName = $vm.Name
    $vms.HostName = $vm.guest.hostname
    $vms.OverallCpuUsage = $vm.summary.quickStats.OverallCpuUsage
    $vms.VMState = $vm.summary.runtime.powerState
    $vms.TotalCPU = $vm.summary.config.numcpu
    $vms.TotalMemory = $vm.summary.config.memorysizemb
    $vms.MemoryUsage = $vm.summary.quickStats.guestMemoryUsage
    $vms.TotalNics = $vm.summary.config.numEthernetCards
    $vms.ToolsStatus = $vm.guest.toolsstatus
    $vms.ToolsVersion = $vm.config.tools.toolsversion
    $Report += $vms
}
$Report | ConvertTo-Html –title "Virtual Machine information" –body "<H2>Virtual Machine information.</H2>" -head "<link rel='stylesheet' href='style.css' type='text/css' />" | Out-File -Append $filelocation



# Get all datastores and put them in alphabetical order#

function Get-DSDevice($dsImpl)
{
		$ds = Get-View -Id $dsImpl.Id
		$esx = Get-View $ds.Host[0].Key
		$hss = Get-View $esx.ConfigManager.StorageSystem

		foreach($mount in $hss.FileSystemVolumeInfo.MountInfo){
		    if($mount.volume.name -eq $ds.Info.Name){
			switch($mount.Volume.Type){
			"VMFS" {
				foreach($ext in $mount.Volume.Extent){
					if($mount.volume.name -eq $ds.Info.Name){
						$device =$ext.DiskName
					}
				}
			  }
			"NFS" {
			    $device = $mount.Volume.RemoteHost + ":" + $mount.Volume.RemotePath
			  }
			}
		  }
		}
	$device
}

$datastores = get-vmhost  | Get-Datastore | Sort-Object Name
$myColCurrent = @()

foreach ($store in $datastores){
	$myObj = "" | Select-Object Name, CapacityGB, UsedGB, PercFree, Type, ID, Accessible
	$myObj.Name = $store.Name
	$myObj.CapacityGB = "{0:n2}" -f ($store.capacityMB/1kb)
	$myObj.UsedGB = "{0:N2}" -f (($store.CapacityMB - $store.FreeSpaceMB)/1kb)
	$myObj.PercFree = "{0:N}" -f (100 * $store.FreeSpaceMB/$store.CapacityMB)
	$myObj.Type = $store.Type
	$temp = Get-View -Id $store.Id
	$myObj.ID = Get-DSDevice $store
	$myObj.Accessible = $store.Accessible
	$myColCurrent += $myObj
}


# Export the output
$myColCurrent | ConvertTo-Html –title "Datastore Information" –body "<H2>Datastore Information.</H2>" -head "<link rel='stylesheet' href='style.css' type='text/css' />" | Out-File -Append $filelocation

#LUN ID

$report1 = @()
$vms = Get-VM |Sort Name -Descending | Get-View
foreach($vm in $vms){
  foreach($dev in $vm.Config.Hardware.Device){
    if(($dev.gettype()).Name -eq "VirtualDisk"){
	  if(($dev.Backing.CompatibilityMode -eq "physicalMode") -or 
	     ($dev.Backing.CompatibilityMode -eq "virtualMode")){
	    $row = "" | select VMName, HDDeviceName, HDFileName, HDMode
          $row.VMName = $vm.Name
	    $row.HDDeviceName = $dev.Backing.DeviceName
	    $row.HDFileName = $dev.Backing.FileName
	    $row.HDMode = $dev.Backing.CompatibilityMode
	    $report1 += $row
	  }
	}
  }
}
$report1 | ConvertTo-Html –title "Virtual Machine information" –body "<H2>RDM information.</H2>" -head "<link rel='stylesheet' href='style.css' type='text/css' />" | Out-File -Append $filelocation

if ($filelocation -ne $null){
    #Send e-mail message
    $subject="IMC VMware Health Status"
    

    $body ="
    <p> Hi,
    <p> Attached is a report of the IMC VMware environments<br>
    This has been checked on <b>NL1IMCVCSA01.mgmt.global, NL2IMCVCSA01</b> and <b>NL2IMCVCSA02.mgmt.global</b>.<br>
    <p>
    <p> 
    <p>
    <p> Best regards,<br>
    inlumi Infrastructure team <br>

    <p> This script has been ran on: <b>$servername</b> <br>
    "

    ## Send the e-mail
    Send-Mailmessage -smtpServer $smtpServer -from $from -to $to -subject $subject -body $body -bodyasHTML -priority High -Attachments "c:\VMReports\VMbuild.htm"
    sleep 5
}





